package bashima.cs.unc.seus.Util;

import android.util.Log;

import bashima.cs.unc.seus.constant.Constant;

/**
 * Created by stephen on 8/30/2017.
 *
 * Solves linear systems for x of the form Ax = b
 */

public class solveLinearSystem {

    private LUPDecompWrapper ADecomp;
    /**
     * Optimizes for the ssolution of Ax = b for x.
     * @param A
     */
    public solveLinearSystem(double[][] A) {
        this.ADecomp = LUPDecomp(A);
    }

    /**
     * Can only be called if initialized
     * @param b
     * @return
     */
    public double[] fastSolveLinearSystemLUPDecomp(double[] b) {

        if(this.ADecomp != null) {
            // Project b onto pivots
            double[] projected_b = matrixVectorMultiplication(this.ADecomp.P, b);

            // Solve Ly = Pb, y = Ux via forward substitution
            double[] intermediate_state = forwardBackSubstitutionSolver(this.ADecomp.L, projected_b, 1);

            // Solve Ux = y
            return forwardBackSubstitutionSolver(this.ADecomp.U, intermediate_state, 0);
        }

        Log.e(Constant.TAG, "State Matrix invalid. Unable to solve system.");
        return null;
    }

    /**
     * Solves Ax = b for x using LUP decomposition / Crout's method. Public method.
     * @param A
     * @param b
     * @return
     */
    public double[] solveLinearSystemLUPDecomp(double[][] A, double[] b) {
        int numRowA;
        int numColA;
        int numRowb;

        // Check for valid dimensions
        numRowA = A.length;
        numRowb = b.length;
        if(numRowA < 1 || numRowb < 1) {
            Log.e(Constant.TAG, "solveLinearSystemLUPDecomp: invalid input dimensions.");
            return null;
        }
        numColA = A[0].length;
        if(numColA < 1) {
            Log.e(Constant.TAG, "solveLinearSystemLUPDecomp: invalid input dimensions.");
            return null;
        }

        // Check for dimension mismatch
        if(numColA != numRowb) {
            Log.e(Constant.TAG, "solveLinearSystemLUPDecomp: input dimension mismatch.");
            return null;
        }

        // Compute LUP Decomposition of A
        LUPDecompWrapper aLUPDecomp = LUPDecomp(A);

        // Project b onto pivots
        double[] projected_b = matrixVectorMultiplication(aLUPDecomp.P, b);

        // Solve Ly = Pb, y = Ux via forward substitution
        double[] intermediate_state = forwardBackSubstitutionSolver(aLUPDecomp.L, projected_b, 1);

        // Solve Ux = y
        return forwardBackSubstitutionSolver(aLUPDecomp.U, intermediate_state, 0);
    }

    /**
     * Computes LUP decomposition of input A
     * @param A
     * @return
     */
    private LUPDecompWrapper LUPDecomp(double[][] A) {
        int numRows;
        int numCols;
        double[][] U;
        double[][] L;
        double[][] P;

        // Check to make sure there are valid dimensions
        numRows = A.length;
        if(numRows < 1) {
            Log.e(Constant.TAG, "LUPDecomp: invalid matrix dimensions.");
            return null;
        }
        numCols = A[0].length;
        if(numCols < 1) {
            Log.e(Constant.TAG, "LUPDecomp: invalid matrix dimensions.");
            return null;
        }

        Log.d(Constant.TAG, "A Dimensions: " + Integer.toString(A.length) + " x " + Integer.toString(A[0].length));
        Log.d(Constant.TAG, "A[0]: " + Double.toString(A[0][0]) + ", " + Double.toString(A[0][1]) + ", " + Double.toString(A[0][2]) + ", ");
        Log.d(Constant.TAG, "A[1]: " + Double.toString(A[1][0]) + ", " + Double.toString(A[1][1]) + ", " + Double.toString(A[1][2]) + ", ");
        Log.d(Constant.TAG, "A[2]: " + Double.toString(A[2][0]) + ", " + Double.toString(A[2][1]) + ", " + Double.toString(A[2][2]) + ", ");

        // Initialization
        U = new double[numRows][numRows];
        L = new double[numRows][numRows];
        P = new double[numRows][numRows];

        for(int i = 0; i < numRows; i++) {

            // Copy A into the upper matrix for Gaussian Elimination
            for(int j = 0; j < numCols; j++) {
                U[i][j] = A[i][j];
            }

            // The lower matrix and permutation matrices start off as identity
            L[i][i] = 1;
            P[i][i] = 1;
        }

        for(int i = 0; i < numRows; i++) {

            // Find the first row with a nonzero entry in the ith entry.
            if(U[i][i] == 0) {

                for(int j = i + 1; j < numRows; j++) {

                    // Non zero entry found; update permutation and upper matrices
                    if(U[j][i] != 0) {

                        // Swap pivots in permutation matrix
                        double[] entry_i = P[i];
                        double[] entry_j = P[j];
                        P[i] = entry_j;
                        P[j] = entry_i;

                        // swap pivots in upper matrix
                        entry_i = U[i];
                        entry_j = U[j];
                        U[i] = entry_j;
                        U[j] = entry_i;

                        // Only need to find first row that has nonzero entry in ith entry
                        break;
                    }
                }
            }

            // Gaussian Elimination to create upper and lower matrices
            for(int j = 0; j < i - 1; j++) {

                // To avoid going to infinity
                if(U[j][j] != 0) {

                    L[i][j] = U[i][j] / U[j][j];
                    for(int k = 0; k < numCols; k++) {
                        U[i][k] = U[i][k] - (U[j][k] * L[i][j]);
                    }
                }
            }
        }

        // Package and return
        LUPDecompWrapper returnParam = new LUPDecompWrapper(L, U, P);

        // Print out for debugging
        Log.d(Constant.TAG, "L Dimensions: " + Integer.toString(L.length) + " x " + Integer.toString(L[0].length));
        Log.d(Constant.TAG, "L[0]: " + Double.toString(L[0][0]) + ", " + Double.toString(L[0][1]) + ", " + Double.toString(L[0][2]) + ", ");
        Log.d(Constant.TAG, "L[1]: " + Double.toString(L[1][0]) + ", " + Double.toString(L[1][1]) + ", " + Double.toString(L[1][2]) + ", ");
        Log.d(Constant.TAG, "L[2]: " + Double.toString(L[2][0]) + ", " + Double.toString(L[2][1]) + ", " + Double.toString(L[2][2]) + ", ");
        Log.d(Constant.TAG, "U Dimensions: " + Integer.toString(U.length) + " x " + Integer.toString(U[0].length));
        Log.d(Constant.TAG, "U[0]: " + Double.toString(U[0][0]) + ", " + Double.toString(U[0][1]) + ", " + Double.toString(U[0][2]) + ", ");
        Log.d(Constant.TAG, "U[1]: " + Double.toString(U[1][0]) + ", " + Double.toString(U[1][1]) + ", " + Double.toString(U[1][2]) + ", ");
        Log.d(Constant.TAG, "U[2]: " + Double.toString(U[2][0]) + ", " + Double.toString(U[2][1]) + ", " + Double.toString(U[2][2]) + ", ");
        Log.d(Constant.TAG, "P Dimensions: " + Integer.toString(P.length) + " x " + Integer.toString(P[0].length));
        Log.d(Constant.TAG, "P[0]: " + Double.toString(P[0][0]) + ", " + Double.toString(P[0][1]) + ", " + Double.toString(P[0][2]) + ", ");
        Log.d(Constant.TAG, "P[1]: " + Double.toString(P[1][0]) + ", " + Double.toString(P[1][1]) + ", " + Double.toString(P[1][2]) + ", ");
        Log.d(Constant.TAG, "P[2]: " + Double.toString(P[2][0]) + ", " + Double.toString(P[2][1]) + ", " + Double.toString(P[2][2]) + ", ");

        return returnParam;
    }

    /**
     * Compute A * x, where A is a matrix and x is a vector
     * @param A
     * @param x
     * @return
     */
    private double[] matrixVectorMultiplication(double[][] A, double[] x) {
        int numRowsA;
        int numColsA;
        int numRowsx;
        double[] returnParam;

        // Check to make sure there are valid dimensions
        numRowsA = A.length;
        numRowsx = x.length;
        if(numRowsA < 1 || numRowsx < 1) {
            Log.e(Constant.TAG, "matrixVectorMultiplication: invalid input dimensions.");
            return null;
        }
        numColsA = A[0].length;
        if(numColsA < 1) {
            Log.e(Constant.TAG, "matrixVectorMultiplication: invalid matrix dimensions.");
            return null;
        }

        // Check for Dimension mismatch
        if(numColsA != numRowsx) {
            Log.e(Constant.TAG, "matrixVectorMultiplication: input dimension mismatch.");
            return null;
        }

        // Compute multiplication
        returnParam = new double[numRowsA];
        for(int i = 0; i < numRowsA; i++) {

            returnParam[i] = 0;
            for(int j = 0; j < numColsA; j++) {
                returnParam[i] = returnParam[i] + (A[i][j] * x[j]);
            }
        }

        return returnParam;
    }

    /**
     * Solves Lx = b, for x using forward substitution. Assumes L is lower triangular
     * @param L
     * @param b
     * @param flag: > 0 for forward substitution, else back substitution
     * @return
     */
    private double[] forwardBackSubstitutionSolver(double[][] L, double[] b, int flag) {
        int numRowL;
        int numColL;
        int numRowb;
        double[] returnParam;

        // Check to make sure there are valid dimensions
        numRowL = L.length;
        numRowb = b.length;
        if(numRowL < 1 || numRowb < 1) {
            Log.e(Constant.TAG, "forwardSubstitutionSolver: invalid input dimensions.");
            return null;
        }
        numColL = L[0].length;
        if(numColL < 1) {
            Log.e(Constant.TAG, "forwardSubstitutionSolver: invalid matrix dimensions.");
            return null;
        }

        // Check for Dimension mismatch
        if(numColL != numRowb) {
            Log.e(Constant.TAG, "forwardSubstitutionSolver: input dimension mismatch.");
            return null;
        }

        // Check and make sure the system is solvable (L is not degenerate)
        if(numRowL != numColL) {
            Log.e(Constant.TAG, "forwardSubstitutionSolver: Input system unsolvable; L is degenerate.");
            return null;
        }
        for(int i = 0; i < numRowL; i++) {

            if(L[i][i] == 0) {
                Log.e(Constant.TAG, "forwardSubstitutionSolver: Input system unsolvable; L is degenerate.");
                return null;
            }
        }

        // Solve
        returnParam = new double[numRowL];

        // Forward substitution
        if(flag > 0) {
            for (int i = 0; i < numRowL; i++) {

                double cumulative_sum = 0;
                for (int j = 0; j < i; j++) {
                    cumulative_sum = cumulative_sum + (L[i][j] * returnParam[j]);
                }
                returnParam[i] = (b[i] - cumulative_sum) / L[i][i];
            }
        }

        // Back substitution
        else {
            for(int i = numRowL - 1; i > -1; i--) {

                double cumulative_sum = 0;
                for(int j = numRowL - 1; j > i; j--) {
                    cumulative_sum = cumulative_sum + (L[i][j] * returnParam[j]);
                }
                returnParam[i] = (b[i] - cumulative_sum) / L[i][i];
            }
        }

        return returnParam;
    }

    /**
     * Wrapper class for holding L, U, P matrices after performing LUP decomposition
     */
    private class LUPDecompWrapper {
        private double[][] L;
        private double[][] U;
        private double[][] P;

        private LUPDecompWrapper(double[][] L, double[][] U, double[][] P) {
            this.L = L;
            this.U = U;
            this.P = P;
        }

    }
}
