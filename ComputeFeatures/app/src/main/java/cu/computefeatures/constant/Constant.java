package cu.computefeatures.constant;

import android.os.Environment;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import cu.computefeatures.feature.GenericCC;

/**
 * Created by stephen on 9/20/2017.
 */

public class Constant {

    // Folder for where we save the file to
    public static String FOLDER_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/SEUS/FeatureTest/";

    // File to compute feature for
    public static String READ_FILE_PATH = FOLDER_PATH + "recording.wav";

    // File to save feature for
    public static String WRITE_FILE_PATH = FOLDER_PATH + "recording.csv";

    // Constants for generating GenericCC features
    public static final int B_GCC = 20;
    public static final int a_GCC = 30;
    public static final int b_GCC = 18;

    // Reading in audio
    public static final int FS = 44100;
    public static final double WINDOW_LENGTH = 0.2; // seconds
    public static final int BUFFER_SIZE = (int) (Math.floor(FS * WINDOW_LENGTH));

    public static double[] convertSignalToDouble(byte[] byteData) {
        int bytePerSample = 2;
        int numSamples = byteData.length / bytePerSample;
        double[] amplitudes = new double[numSamples];

        int pointer = 0;
        for (int i = 0; i < numSamples; i++) {
            short amplitude = 0;
            for (int byteNumber = 0; byteNumber < bytePerSample; byteNumber++) {
                // little endian
                amplitude |= (short) ((byteData[pointer++] & 0xFF) << (byteNumber * 8));
            }
            amplitudes[i] = amplitude;
        }

        return amplitudes;
    }
}
