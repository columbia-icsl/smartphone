package cu.computefeatures.feature;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;

import cu.computefeatures.constant.Constant;

/**
 * Created by stephen on 9/20/2017.
 */

public class GenericCC {
    private final static String TAG = "GenericCC";
    private double sampling_freq;
    private double window_len;
    private double time_shift;
    private int B_self;
    private int a_self;
    private int b_self;
    private int frame_len;
    private int nfft;
    private FFT mfft;
    private int n, m;
    private int dt1, dt2;
    private int r_len, r1_len;

    private String wintype = "HAMMING";
    private double[] window;

    // Logging
    private static int log_counter = 0;

    /**
     * Constructor
     * @param fs is the sampling frequency (Hz)
     * @param wl is the window length (s)
     * @param ts is the analysis frame shift (ms)
     * @param B
     * @param a
     * @param b
     */
    public GenericCC(double fs, double wl, double ts, int B, int a, int b)
    {
        sampling_freq = fs;
        window_len = wl;
        time_shift = ts;
        B_self = B;
        a_self = a;
        b_self = b;

        // Check for valid input parameters
        if(B <= b)
        {
            Log.e(TAG, "B <= b: invalid.");
        }

        // Compute number of samples per frame
        frame_len = (int) Math.floor(sampling_freq * window_len);
        if(frame_len < 1)
        {
            Log.e(TAG, "Invalid sampling frequency and/or window length.");
        } else {

            // Otherwise, generate window function
            WindowFunction window_func = new WindowFunction();
            window_func.setWindowType(wintype);
            window = window_func.generate(frame_len);

            // Compute nfft and initialize fft
            nfft = (int) Math.pow(2, PreProcess.nextpow2(frame_len));
            mfft = new FFT(nfft);

            // Compute parameters
            n = (nfft / 2) + 1;
            m = (int) Math.max(1, Math.floor(((double) a_self) * n / 100));
            dt1 = (int) Math.max(Math.floor(((double) m) / b_self), 1);
            dt2 = (int) Math.max(Math.floor(((double)(n - m)) / (B_self - b_self)), 1);

            // Error checking dt1 and dt2
            if(!(dt2 > 0 && dt1 > 0))
            {
                Log.d(TAG, "dt1 and dt2 invalid.");
            }

            // Compute accumulation vector params
            r1_len = (b_self) * dt1;
            r_len = r1_len + ((B_self - b_self) * dt2);
        }
    }

    /**
     * @param audio_samples: audio signal
     * @return
     */
    public double[] generate_features(double[] audio_samples)
    {
        int frame_count;
        double[][] psd;
        double[] mean_psd, features;
        int i, j, k;

        // Power spectral density
        Log.d("GenericCC", "audio sample length: " + audio_samples.length);
        Log.d("GenericCC", "r length: " + r_len);
        Log.d("GenericCC", "frame len: " + frame_len);
        psd = power_spectral_density(audio_samples, r_len);
        Log.d("GenericCC", "Num PSD Freq: " + psd[0].length);
        Log.d("GenericCC", "dt1: " + dt1);
        Log.d("GenericCC", "dt2: " + dt2);
        frame_count = psd.length;
        //Log.d(TAG, Arrays.toString(psd[0]));
        Log.d("GenericCC", "frame count: " + frame_count);

        for(i = 0; i < frame_count; i++) {

            // Scaling the first values by dt1.
            for(j = 0; j < r1_len; j++) {
                psd[i][j] = psd[i][j] * dt1;
            }

            // Scaling the rest by dt2
            for(j = r1_len; j < r_len; j++) {
                psd[i][j] = psd[i][j] * dt2;
            }
        }

        // Compute Mean
        mean_psd = new double[r_len];
        for(j = 0; j < r_len; j++)
        {
            for(i = 0; i < frame_count; i++)
            {
                mean_psd[j] = mean_psd[j] + psd[i][j];
            }
            mean_psd[j] = mean_psd[j] / frame_count;
        }

        // Accumulation to create feature bins
        features = new double[B_self];
        i = 0;
        for(k = 0; k < b_self; k++)
        {
            for(j = 0; j < dt1; j++)
            {
                features[k] = features[k] + mean_psd[i];
                i = i + 1;
            }
        }

        for(k = b_self; k < B_self; k++)
        {
            for(j = 0; j < dt2; j++)
            {
                features[k] = features[k] + mean_psd[i];
                i = i + 1;
            }
        }

        // dB scale
        //for(k = 0; k < B_self; k++) {
        //    features[k] = 20 * Math.log10(features[k]);
        //}

        //Log.d(TAG, Arrays.toString(features));
        return features;
    }

    /**
     * Computes power spectral density after partitioning data into frames specified by the parameters
     * fed into the constructor
     * @param signal
     * @param nFreq: number of frequencies to return; should be less than equal to nfft.
     * @return psd: the power spectral density of the signal, returning only the number of bins requested
     */
    private double[][] power_spectral_density(double[] signal, int nFreq)
    {
        double[][] frames, paddedFrames, frames_re, frames_im, psd;
        int totalFrames;

        // Check if nfreq is less than equal to nfft
        if(nFreq > nfft)
        {
            Log.e(TAG, "Number of freq bins requested should be less than time window");
            return null;
        }

        // Partition by windows
        frames = PreProcess.vec2frames(signal, (int) frame_len);
        totalFrames = frames.length;

        // Padding
        paddedFrames = new double[totalFrames][nfft];
        for(int i = 0; i < totalFrames; i++)
        {
            // Apply the window before padding
            for(int j = 0; j < frame_len; j++)
            {
                frames[i][j] = frames[i][j] * window[j];
            }
            paddedFrames[i] = Arrays.copyOf(frames[i], nfft);
        }

        // Compute power spectral density
        frames_re = paddedFrames;
        frames_im = new double[totalFrames][nfft];
        psd = new double[totalFrames][nFreq];
        for(int i = 0; i < totalFrames; i++)
        {
            // Take fft of window
            mfft.fft(frames_re[i], frames_im[i]);

            // Compute power spectral density
            psd[i][0] = ((1 / ((double) sampling_freq * nfft)) * (Math.pow(frames_re[i][0], 2) + Math.pow(frames_im[i][0], 2)));
            for(int j = 1; j < nFreq; j++)
            {
                // Multiple all frequencies besides dc to account for positive and negative frequencies.
                psd[i][j] = (2 * (1 / ((double) sampling_freq * nfft)) * (Math.pow(frames_re[i][j], 2) + Math.pow(frames_im[i][j], 2)));

                // Normalizing psd by sampling frequency
                //psd[i][j] = (2 * (1 / ((double) sampling_freq * nfft)) * (Math.pow(frames_re[i][j], 2) + Math.pow(frames_im[i][j], 2)));
            }
        }

        /*// Log the PSD
        try {

            // Setup
            FileOutputStream outputStreamPSD = new FileOutputStream(Constant.FOLDER_PATH + Integer.toString(log_counter) + ".csv");
            OutputStreamWriter outputStreamWriterPSD = new OutputStreamWriter(outputStreamPSD);
            String outputString = "";

            // Convert PSD into string format
            for(int i = 0; i < psd.length; i++) {
                for(int j = 0; j < psd[i].length; j++) {
                    outputString = outputString + Double.toString(psd[i][j]) + ",";
                }
                outputString = outputString + "\n";
            }

            // Log
            outputStreamWriterPSD.write(outputString);

            // Cleanup
            outputStreamWriterPSD.close();
            outputStreamPSD.close();

            // Increment counter for next psd computed
            log_counter = log_counter + 1;

        } catch(Exception e) {
            Log.e("power_spectral_density", e.toString());
        }*/

        return psd;
    }

}

