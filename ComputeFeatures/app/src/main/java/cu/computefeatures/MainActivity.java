package cu.computefeatures;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import cu.computefeatures.constant.Constant;
import cu.computefeatures.feature.GenericCC;
import cu.computefeatures.feature.PreProcess;

public class MainActivity extends AppCompatActivity {

    private FileOutputStream outputStream;
    private OutputStreamWriter outputStreamWriter;
    private GenericCC genericCC_features;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Read audio
        double[] audioData = readAudioFromFile(Constant.READ_FILE_PATH);
        Log.i("Main", "Length data: " + audioData.length);

        // Set up logging folder if it doesn't exist already
        File directory = new File(Constant.FOLDER_PATH);
        if(!directory.exists()) {
            Log.d("Main", "Logging folder does not exist; creating....");
            directory.mkdirs();
        }

        // Set up writer to writer features
        try {
            outputStream = new FileOutputStream(Constant.WRITE_FILE_PATH);
            outputStreamWriter = new OutputStreamWriter(outputStream);
        } catch (Exception e) {
            Log.e("Main", e.toString());
        }

        // Setup genericCC calculator
        genericCC_features = new GenericCC(Constant.FS, Constant.WINDOW_LENGTH, 0, Constant.B_GCC, Constant.a_GCC, Constant.b_GCC);

        // Compute
        computeAndLogFeatures(audioData);

        // Cleanup
        try {
            outputStreamWriter.close();
            outputStream.close();
        } catch (Exception e) {
            Log.e("Main", e.toString());
        }

    }

    /**
     * Read audio from file and return the data read
     * @param filename
     * @return
     */
    private double[] readAudioFromFile(String filename) {

        double [] audio_data = new double[1];
        File audioFile = new File(filename);
        int fileSize = (int) audioFile.length();

        byte [] raw_audio = new byte[fileSize];

        try {
            FileInputStream inputStream = new FileInputStream(audioFile);

            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, Constant.FS);
            DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);      //  Create a DataInputStream to read the audio data from the saved file

            int i = 0;   //  Read the file into the "audio" array
            while (dataInputStream.available() > 0)

            {
                raw_audio[i] = dataInputStream.readByte();     //  This assignment does not reverse the order
                i++;
            }

            dataInputStream.close();
            inputStream.close();

            // Convert to double
            audio_data = Constant.convertSignalToDouble(raw_audio);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return audio_data;
    }

    /**
     * Computes and logs features from data
     * @param data
     */
    private void computeAndLogFeatures(double[] data) {

        int numWindows = (int) Math.floor(data.length / Constant.BUFFER_SIZE);
        double[] window = new double[Constant.BUFFER_SIZE];
        String featureString = "";

        Log.d("computeAndLogFeatures", "numWindows: " + numWindows);
        Log.d("computeAndLogFeatures", "Buffer Size" + Constant.BUFFER_SIZE);

        // Compute features
        for(int i = 0; i < numWindows; i++) {

            // Get window
            System.arraycopy(data, i * Constant.BUFFER_SIZE, window, 0, Constant.BUFFER_SIZE);

            // Generate feature
            double[] NBIP_features = genericCC_features.generate_features(window);

            // Generate String
            for(int j = 0; j < NBIP_features.length; j++) {
                featureString = featureString + Double.toString(NBIP_features[j]) + ",";
            }
            featureString = featureString + "\n";
        }

        try {
            // Log
            outputStreamWriter.write(featureString);
        } catch (Exception e) {
            Log.e("Main", e.toString());
        }
    }
}
